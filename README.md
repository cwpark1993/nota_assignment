**구조 설명**

- Jetpack CameraX API 사용 
- Tensorflow Lite v1.14 사용 

Input : Bitmap ( CameraX ImageAnalyzer API 사용 )

Process : Tensorflow Lite API

Output :  Label, Probablity



**추가 구현**

Image Classification 수행 결과에 따라 구글 이미지 검색을 수행합니다.

화면 밑부분에 구글 이미지 검색 결과가 나타납니다.

