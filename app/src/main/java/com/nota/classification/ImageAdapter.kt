package com.nota.classification

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nota.classification.databinding.ListitemImgBinding

/** 구글 이미지 검색 후 이미지 결과를 보여줄 RecyclerView Adapter */
class ImageAdapter: RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {

    private val listData = ArrayList<String>()

    fun setData(data: ArrayList<String>){
        Log.d("ImageAdapter", "data Size : " + data.size)
        listData.clear()
        listData.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(ListitemImgBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val img = listData[position]
        img.let { holder.bind(it) }
    }

    class ImageViewHolder(private val binding: ListitemImgBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(item: String){
            binding.apply {
                Glide.with(iv).load(item).into(iv)
            }
        }
    }

}