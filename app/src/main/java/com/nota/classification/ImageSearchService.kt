package com.nota.classification

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

// 구글 이미지검색 구현을 위한 Retrofit API
interface ImageSearchService {

    @GET("search?tbm=isch")
    fun getStringResponse(@Query("q") keyword:String): Call<String>

    companion object {

        private const val BASE_URL = "https://www.google.com/"

        fun create(): ImageSearchService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
                .create(ImageSearchService::class.java)
        }
    }
}