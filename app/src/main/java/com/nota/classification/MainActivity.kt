package com.nota.classification

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.*
import android.media.Image
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.nota.classification.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

typealias bitmapListener = (bitmap: Bitmap, orientation: Int) -> Unit

class MainActivity : AppCompatActivity() {

    private lateinit var cameraExecutor: ExecutorService
    private lateinit var classifier: Classifier
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private var searchCounter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        cameraExecutor = Executors.newSingleThreadExecutor()
        classifier = Classifier(this, 3)

        if (allPermissionsGranted()) {
            startCamera()
        } else {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }

        // 이미지 검색 결과 어댑터에 셋팅
        viewModel.searchResult.observe(this, Observer {
            viewModel.getImgAdapter().setData(it)
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults:
        IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startCamera()
            } else {
                // 권한 거절 시 앱 종료
                Toast.makeText(this, "권한 거절됨.", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    // CameraX API 구현
    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        cameraProviderFuture.addListener(Runnable {
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(camera_container.createSurfaceProvider())
                }

            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            val imageAnalyzer = ImageAnalysis.Builder()
                .build()
                .also {
                    // Camera Analyzer Impl
                    it.setAnalyzer(cameraExecutor, ToBitmapAnalyzer { bitmap, orientation ->

                        val results = classifier.recognizeImage(bitmap, orientation)

                        val resultText = getString(R.string.result_text,
                            results[0].title, String.format("%.2f", results[0].prob * 100) + "%",
                            results[1].title, String.format("%.2f", results[1].prob * 100) + "%",
                            results[2].title, String.format("%.2f", results[2].prob * 100) + "%")

                        // Image Classification 수행 결과 UI Post
                        viewModel.outputLabel.postValue(resultText)

                        // SEARCH_DELAY_COUNT 횟수마다 이미지 검색 수행
                        // ( 너무 작은 숫자로 수행 시, 로봇 방지 캡챠 때문에 크롤링 기능 수행 X )
                        searchCounter--
                        if(searchCounter <= 0){
                            searchCounter = SEARCH_DELAY_COUNT
                            viewModel.searchImg(results[0].title)
                        }
                    })
                }

            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(
                    this, cameraSelector, preview, imageAnalyzer)

            } catch(exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(this))

    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    /**  Preview ImageProxy -> Bitmap 으로 변환하여 Callback */
    private class ToBitmapAnalyzer(private val listener: bitmapListener) : ImageAnalysis.Analyzer {

        @ExperimentalGetImage
        override fun analyze(image: ImageProxy) {
            image.image?.let{
                listener(it.toBitmap(), image.imageInfo.rotationDegrees)
            }
            image.close()
        }

        fun Image.toBitmap(): Bitmap {
            val yBuffer = planes[0].buffer // Y
            val uBuffer = planes[1].buffer // U
            val vBuffer = planes[2].buffer // V

            val ySize = yBuffer.remaining()
            val uSize = uBuffer.remaining()
            val vSize = vBuffer.remaining()

            val nv21 = ByteArray(ySize + uSize + vSize)

            //U and V are swapped
            yBuffer.get(nv21, 0, ySize)
            vBuffer.get(nv21, ySize, vSize)
            uBuffer.get(nv21, ySize + vSize, uSize)

            val yuvImage = YuvImage(nv21, ImageFormat.NV21, this.width, this.height, null)
            val out = ByteArrayOutputStream()
            yuvImage.compressToJpeg(Rect(0, 0, yuvImage.width, yuvImage.height), 50, out)
            val imageBytes = out.toByteArray()
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        }
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val REQUEST_CODE_PERMISSIONS = 10
        private const val SEARCH_DELAY_COUNT = 100
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }
}