package com.nota.classification

import android.app.Activity
import android.graphics.Bitmap
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.support.common.FileUtil
import org.tensorflow.lite.support.common.TensorProcessor
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.image.ops.ResizeOp.ResizeMethod
import org.tensorflow.lite.support.image.ops.ResizeWithCropOrPadOp
import org.tensorflow.lite.support.image.ops.Rot90Op
import org.tensorflow.lite.support.label.TensorLabel
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.util.*
import kotlin.collections.ArrayList

/** Image Classifier 구현 */
class Classifier(activity: Activity, numThreads: Int) {

    private var inputImageBuffer: TensorImage
    private val tfLite: Interpreter
    private val tfLiteOptions = Interpreter.Options()
    private val outputProbabilityBuffer: TensorBuffer
    private val probabilityProcessor: TensorProcessor

    private val labels: List<String>
    private var imageSizeX: Int = 0
    private var imageSizeY: Int = 0

    init {
        val tfLiteModel = FileUtil.loadMappedFile(activity, MODEL_FILE_NAME)
        tfLiteOptions.setNumThreads(numThreads)
        tfLite = Interpreter(tfLiteModel, tfLiteOptions)
        labels = FileUtil.loadLabels(activity, LABEL_FILE_NAME)

        val imageTensorIndex = 0
        val imageShape = tfLite.getInputTensor(imageTensorIndex).shape() // {1, height, width, 3}

        imageSizeY = imageShape[1]
        imageSizeX = imageShape[2]

        val imageDataType = tfLite.getInputTensor(imageTensorIndex).dataType()
        val probabilityTensorIndex = 0
        val probabilityShape = tfLite.getOutputTensor(probabilityTensorIndex).shape() // {1, NUM_CLASSES}
        val probabilityDataType = tfLite.getOutputTensor(probabilityTensorIndex).dataType()

        inputImageBuffer = TensorImage(imageDataType)

        outputProbabilityBuffer =
            TensorBuffer.createFixedSize(probabilityShape, probabilityDataType)

        // Probability 값을 0~255 값으로 Normalize
        probabilityProcessor = TensorProcessor.Builder().add( NormalizeOp(0f, 255f) ).build()
    }

    // 이미지 인식 부분
    fun recognizeImage(bitmap: Bitmap, orientation: Int): List<Recognition>{
        inputImageBuffer = loadImage(bitmap, orientation)

        tfLite.run(inputImageBuffer.buffer, outputProbabilityBuffer.buffer.rewind())

        val labeledProbability: Map<String, Float> =
            TensorLabel(labels, probabilityProcessor.process(outputProbabilityBuffer))
                .mapWithFloatValue

        return getTopKProbability(labeledProbability)
    }

    // 가장 높은 Proba 으로 정렬 후 MAX_RESULTS 갯수만큼 리턴
    private fun getTopKProbability(labelProb: Map<String, Float>): List<Recognition> {

        val pq = PriorityQueue<Recognition>(
            MAX_RESULTS,
            Comparator<Recognition> { lhs, rhs ->
                rhs.prob.compareTo(lhs.prob)
            })

        for ((key, value) in labelProb) {
            pq.add(Recognition("" + key, key, value))
        }

        val recognitions = ArrayList<Recognition>()
        val recognitionsSize = pq.size.coerceAtMost(MAX_RESULTS)

        for (i in 0 until recognitionsSize) {
            pq.poll()?.let { recognitions.add(it) }
        }
        return recognitions
    }

    // 이미지 로드 및 전처리
    private fun loadImage(bitmap: Bitmap, sensorOrientation: Int): TensorImage {
        inputImageBuffer.load(bitmap)

        val cropSize = bitmap.width.coerceAtMost(bitmap.height)
        val numRotation = sensorOrientation / 90
        val imageProcessor: ImageProcessor = ImageProcessor.Builder()
            // 이미지를 정사각형으로 Crop
            .add(ResizeWithCropOrPadOp(cropSize, cropSize))
            // Input Shape 에 맞춰 Image Resize
            .add(ResizeOp(imageSizeX, imageSizeY, ResizeMethod.NEAREST_NEIGHBOR))
            // 사진 Orientation 조정
            .add(Rot90Op(numRotation))
            // Input Normalize 0 ~ 1
            .add(NormalizeOp(0f, 1f))
            .build()
        return imageProcessor.process(inputImageBuffer)
    }

    companion object {
        const val MAX_RESULTS = 3
        const val MODEL_FILE_NAME = "classification_model.tflite"
        const val LABEL_FILE_NAME = "classification_model_label.txt"
    }
}