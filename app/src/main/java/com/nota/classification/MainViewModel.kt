package com.nota.classification

import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jsoup.Jsoup

class MainViewModel: ViewModel() {

    private val imgAdapter = ImageAdapter()

    val searchResult: MutableLiveData<ArrayList<String>> by lazy {
        MutableLiveData<ArrayList<String>>()
    }

    val outputLabel: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    fun getImgAdapter():ImageAdapter{return imgAdapter}

    fun searchImg(keyword: String){
        viewModelScope.launch(Dispatchers.IO) {
            val imgList = ArrayList<String>()
            val output = ImageSearchService.create().getStringResponse(keyword).execute()
            output.body()?.let {
                val parsed = Jsoup.parse(output.body()).select("img")

                for(img in parsed){
                    val imgUrl = img.attr("src")

                    if(imgUrl.startsWith("https"))
                        imgList.add(img.attr("src"))
                }
            }
            // UI 쓰레드에서 수행하기 위해 검색결과 Post
            searchResult.postValue(imgList)
        }
    }

    companion object {

        @BindingAdapter("setAdapter")
        @JvmStatic
        fun setAdapter(view : RecyclerView, adapter : RecyclerView.Adapter<RecyclerView.ViewHolder> ){
            view.adapter = adapter
        }

    }

}