package com.nota.classification


data class Recognition(val id:String, val title:String, val prob:Float) {

    override fun toString(): String {
        return "Recognition(id=$id, title='$title', prob=$prob)"
    }
}