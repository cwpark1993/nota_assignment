package com.nota.classification

import org.jsoup.Jsoup
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun googleSearchTest() {
        val output = ImageSearchService.create().getStringResponse("keyword").execute()
        val parsed = Jsoup.parse(output.body()).select("img")

        for(img in parsed){
            val imgUrl = img.attr("src")

            if(imgUrl.startsWith("https"))
                print(img.attr("src") + "\n")
        }
    }
}